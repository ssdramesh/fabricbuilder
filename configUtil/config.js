var electronDirectory = require("electron-directory");
var read = require("read-data").sync;
var write = require("write-data").sync;
var sync = require("promise-synchronizer");
var fs = require("fs-extra");
var _ = require("underscore");

let getSettingsFilePath = new Promise((resolve, reject) => {
  electronDirectory(__dirname)
    .then(function(electronDirectoryInstance){
      electronDirectoryInstance.getApplicationPath("/../config/config.json")
        .then( function(settingsFilePath) {
          resolve(settingsFilePath);
        })
    })
})

let settingsFile;
try {
  settingsFile = sync(getSettingsFilePath);
} catch(err) {
  console.error(err);
}

function readScriptPathSetting() {
  return read(settingsFile).scriptPath;
}

function readModelPathSetting() {
  return read(settingsFile).modelPath;
}

function updateProject(project) {
  var settings = read(settingsFile);
  var projects = settings.projects;
  projects = _.without(projects, _.findWhere(projects, {projectName:project.projectName}));
  projects.push(project);
  settings.projects = projects;
  write(settingsFile, settings);
}

function removeProject(projectName) {
  var settings = read(settingsFile);
  var projects = settings.projects;
  projects = _.without(projects, _.findWhere(projects, {projectName:projectName}));
  settings.projects = projects;
  write(settingsFile, settings);
}

function getProjects() {
  var settings = read(settingsFile);
  return settings.projects;
}

function getProject(projectName) {
  return _.findWhere(this.getProjects(), {projectName:projectName});
}

function addNewProject(projectName, projectPath) {
  var settings = read(settingsFile);
  if ( this.getProject(projectName) ) {
    dialog.showErrorBox("This project already exists", "Specify a new name and location for your business network");
  } else {
    fs.mkdirSync("./_NetworkModels/" + projectName);
    var newProject = {
      projectName: projectName,
      status: "new",
      modelsFolder: path.resolve("./_NetworkModels") + "/" + projectName,
      generationProjectPath: projectPath + "/" + projectName
    }
  }
  settings.projects.push(newProject);
  write(settingsFile, settings);
}

function getProjectStatus(projectName) {
  var project = this.getProject(projectName);
  return project.status;
}

function changeProjectStatus(projectName, newStatus) {
  var project = this.getProject(projectName);
  //TODO - Check status transitions
  project.status = newStatus;
  this.updateProject(project);
}

function addModelingDetails(projectName, modelingDetails) {
  var project = this.getProject(projectName);
  project.modelJSONFile = modelingDetails.modelJSONFile;
  project.modelParsedJSONFile = modelingDetails.modelParsedJSONFile;
  this.updateProject(project);
}

function addDeployedChannelName(projectName, cfChannelDetails) {
  var project = this.getProject(projectName);
  project.deployedChannelDetails = cfChannelDetails;
  this.updateProject(project);
}

function addDeployedChaincodes(projectName, deployedChaincodes) {
  var project = this.getProject(projectName);
  project.deployedChaincodes = deployedChaincodes;
  this.updateProject(project);
}

function getDeployedChaincodes(projectName) {
  var project = this.getProject(projectName);
  if ( project && project.status === "deployed" ) {
    return project.deployedChaincodes;
  } else {
    return;
  }
}

exports.readScriptPathSetting = readScriptPathSetting;
exports.readModelPathSetting = readModelPathSetting;
exports.getProjects = getProjects;
exports.getProject = getProject;
exports.removeProject = removeProject;
exports.updateProject = updateProject;
exports.addNewProject = addNewProject;
exports.getProjectStatus = getProjectStatus;
exports.changeProjectStatus = changeProjectStatus;
exports.addModelingDetails = addModelingDetails;
exports.addDeployedChannelName = addDeployedChannelName;
exports.addDeployedChaincodes = addDeployedChaincodes;
exports.getDeployedChaincodes = getDeployedChaincodes;
