#!/usr/bin/env bash

TEMPLATE_UI_DIR=$1
BIZNET_UI_DIR=$2
BIZNET_FOLDER_NAME=$3

#Copy the Setup UI template to target business network ui folder
cp -Rfi "${TEMPLATE_UI_DIR}/SampleSetup" "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
cd "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup"
find . -name "*.ubak" -type f -delete

#Replace business network attributes in the target ui project files
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/package.json"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/manifest.yaml"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/app.js"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/manifest.json"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/index.html"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/Component.js"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/view/Selection.view.xml"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/Selection.controller.js"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/controller/BaseController.js"
sed -i.ubak 's/<sample-network>/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/i18n/i18n.properties"
sed -i.ubak 's/sample-network/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_UI_DIR}/${BIZNET_FOLDER_NAME}Setup/webapp/util/restBuilder.js"

exit 0
