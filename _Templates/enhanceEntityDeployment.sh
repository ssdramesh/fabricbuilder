#!/usr/bin/env bash

BIZNET_DEPLOY_DIR=$1
ENTITY_NAME=$2
ENTITY_NAME_LOWER=$3
ENTITY_TYPE=$4
IS_FIRST=$5

cd "${BIZNET_DEPLOY_DIR}"

#Deployment Descriptor for specified entity
ENTITY_CHAINCODE_TEMPLATE="{
  \"name\":\"${ENTITY_NAME_LOWER}${ENTITY_TYPE}\",
  \"chaincodeId\": \"<CHAINCODE_ID_${ENTITY_NAME}${ENTITY_TYPE}>\",
  \"chaincodeVersion\": \"/<CHAINCODE_VERSION_${ENTITY_NAME}${ENTITY_TYPE}>\",
  \"methods\": [{
    \"name\": \"addNew${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"addNew\",
    \"path\": \"/${ENTITY_NAME_LOWER}${ENTITY_TYPE}\",
    \"httpMethod\":\"POST\",
    \"successMessage\": \"Creation of new ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"update${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"update\",
    \"path\": \"/${ENTITY_NAME_LOWER}${ENTITY_TYPE}/update\",
    \"httpMethod\":\"POST\",
    \"successMessage\": \"Update of ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"readAll${ENTITY_NAME}${ENTITY_TYPE}s\",
    \"type\":\"readAll\",
    \"path\": \"/${ENTITY_NAME_LOWER}${ENTITY_TYPE}\",
    \"httpMethod\":\"GET\",
    \"successMessage\": \"Retrieval of all ${ENTITY_NAME}${ENTITY_TYPE}s successful\"
  }, {
    \"name\": \"read${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"read\",
    \"path\": \"/${ENTITY_NAME_LOWER}${ENTITY_TYPE}/{id}\",
    \"httpMethod\":\"GET\",
    \"successMessage\": \"Read ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"remove${ENTITY_NAME}${ENTITY_TYPE}\",
    \"type\":\"remove\",
    \"path\": \"/${ENTITY_NAME_LOWER}${ENTITY_TYPE}/{id}\",
    \"httpMethod\":\"DELETE\",
    \"successMessage\": \"Removal of ${ENTITY_NAME}${ENTITY_TYPE} successful\"
  }, {
    \"name\": \"removeAll${ENTITY_NAME}${ENTITY_TYPE}s\",
    \"type\":\"removeAll\",
    \"path\": \"/${ENTITY_NAME_LOWER}${ENTITY_TYPE}/clear\",
    \"httpMethod\":\"DELETE\",
    \"successMessage\": \"Removal of all ${ENTITY_NAME}${ENTITY_TYPE}s successful\"
  }]
}"

#Append to backup file
if [[ "${IS_FIRST}" == "y" ]]; then
  echo "$ENTITY_CHAINCODE_TEMPLATE" >> ${BIZNET_DEPLOY_DIR}/entitiesDeployment.dbak
else
  echo ",
   $ENTITY_CHAINCODE_TEMPLATE" >> ${BIZNET_DEPLOY_DIR}/entitiesDeployment.dbak
fi

exit 0
