#!/usr/bin/env bash

TEMPLATE_DEPLOYMENT_DIR=${1}
BIZNET_DEPLOY_DIR=${2}
BIZNET_SCRIPTS_DIR=${3}
BIZNET_UI_DIR=${4}
BIZNET_FOLDER_NAME={5}

#Check and delete previous states of configuration files
find . -name "*.dbak" -type f -delete

#Copy deployment descriptor skeleton
cp -f "${TEMPLATE_DEPLOYMENT_DIR}/_initTemplateChaincodeDetails.json" "${BIZNET_DEPLOY_DIR}/chaincodeDetails.json"

#Copy channel service details
cp -rf "${TEMPLATE_DEPLOYMENT_DIR}/ChannelDetails" "${BIZNET_DEPLOY_DIR}"

exit 0
