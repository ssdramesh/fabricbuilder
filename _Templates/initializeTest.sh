#!/usr/bin/env bash

TEMPLATE_TEST_DIR=${1}
BIZNET_TEST_DIR=${2}
BIZNET_FOLDER_NAME=${3}
BIZNET_DIR=${4}

#Check and delete previous states of configuration files
cd "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup"
find . -name "*.tbak" -type f -delete

#Copy environment, collection and script files to business network
cp -fi "${TEMPLATE_TEST_DIR}/newman/CleanAllAndSetup/sample-net-env.postman_environment.json" "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"

#Replace variables
sed -i.tbak 's/sample-net-env/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json"

exit 0
