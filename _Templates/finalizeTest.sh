#!/usr/bin/env bash

BIZNET_FOLDER_NAME=${1}
BIZNET_TEST_DIR=${2}
BIZNET_SCRIPTS_DIR=${3}

#Close environment.json file
echo "  ],
  \"_postman_variable_scope\": \"environment\",
  \"_postman_exported_at\": \"2018-07-13T08:25:14.314Z\",
  \"_postman_exported_using\": \"Postman/6.1.4\"
}" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityEnv.tbak

#Append the contents of test environment and script backup files target test environment and script files
cat ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityEnv.tbak >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/${BIZNET_FOLDER_NAME}.postman_environment.json
cat ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityRunScript.tbak >> ${BIZNET_SCRIPTS_DIR}/runCleanAllAndSetup.sh

#Check and delete previous states of configuration files
cd "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup"
find . -name "*.tbak" -type f -delete

exit 0
