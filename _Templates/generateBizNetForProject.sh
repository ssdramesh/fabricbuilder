#!/usr/bin/env bash

# Somehow, jq does not work without this
export PATH=$PATH:/usr/local/bin

cd ${PARENT_DIR}
BIZNET_FOLDER_NAME=${1}
BIZNET_DIR=${2}
BIZNET_SRC_DIR=${BIZNET_DIR}/chaincode
BIZNET_DEPLOY_DIR=${BIZNET_DIR}/deployment
BIZNET_MODEL_DIR=${BIZNET_DIR}/model
BIZNET_SCRIPTS_DIR=${BIZNET_DIR}/scripts
BIZNET_TEST_DIR=${BIZNET_DIR}/test
BIZNET_UI_DIR=${BIZNET_DIR}/ui
#scriptPath
PARENT_DIR=${3}
MDL_PARSED_JSON_FILE=${4}

#Source folders for templates
TEMPLATE_ROOT_DIR="${PARENT_DIR}"
TEMPLATE_DEPLOYMENT_DIR="${TEMPLATE_ROOT_DIR}/_Deployment"
TEMPLATE_TEST_DIR="${TEMPLATE_ROOT_DIR}/_Test"
TEMPLATE_MODEL_DIR="$(dirname ${PARENT_DIR})/_NetworkModels"
TEMPLATE_SCRIPTS_DIR="${TEMPLATE_ROOT_DIR}/_Scripts"
TEMPLATE_UI_DIR="${TEMPLATE_ROOT_DIR}/_UI"
TEMPLATE_ASSET_DIR="${TEMPLATE_ROOT_DIR}/Assets"
TEMPLATE_PARTICIPANT_DIR="${TEMPLATE_ROOT_DIR}/Participants"

#Business Network Folder
if [ ! -d "${BIZNET_DIR}" ]; then
  cd "$(dirname ${BIZNET_DIR})" && mkdir "${BIZNET_FOLDER_NAME}"
fi
cd "${BIZNET_DIR}"

#Chaincode folder
if [ ! -d "${BIZNET_SRC_DIR}" ]; then
  cd "${BIZNET_DIR}" && mkdir chaincode
fi
cd "${BIZNET_DIR}"

#Deployment folder
if [ ! -d "${BIZNET_DEPLOY_DIR}" ]; then
  cd "${BIZNET_DIR}" && mkdir deployment && cd deployment && mkdir ChannelDetails
  bash ${TEMPLATE_ROOT_DIR}/initializeDeployment.sh \
    "${TEMPLATE_DEPLOYMENT_DIR}"\
    "${BIZNET_DEPLOY_DIR}" \
    "${BIZNET_SCRIPTS_DIR}" \
    "${BIZNET_UI_DIR}" \
    "${BIZNET_FOLDER_NAME}"
fi
cd "${BIZNET_DIR}"

#Test folder
if [ ! -d "${BIZNET_TEST_DIR}" ]; then
  cd "${BIZNET_DIR}" && mkdir test && cd test && mkdir newman && cd newman && mkdir CleanAllAndSetup && cd CleanAllAndSetup && mkdir data && mkdir log
  cd "${BIZNET_TEST_DIR}"
  bash ${TEMPLATE_ROOT_DIR}/initializeTest.sh \
    "${TEMPLATE_TEST_DIR}" \
    "${BIZNET_TEST_DIR}"\
    "${BIZNET_FOLDER_NAME}" \
    "${BIZNET_DIR}"
fi
cd "${BIZNET_DIR}"

#Model folder
if [ ! -d "${BIZNET_MODEL_DIR}" ]; then
  cd "${BIZNET_DIR}" && mkdir model
fi
cd "${BIZNET_DIR}"
cp -f "${MDL_PARSED_JSON_FILE}" "${BIZNET_MODEL_DIR}"


#ui folder
if [ ! -d "$BIZNET_UI_DIR" ]; then
  cd "${BIZNET_DIR}" && mkdir ui
  bash ${TEMPLATE_ROOT_DIR}/initializeSetupUI.sh \
    "${TEMPLATE_UI_DIR}"\
    "${BIZNET_UI_DIR}"\
    "${BIZNET_FOLDER_NAME}"
fi
cd "${BIZNET_DIR}"

#scripts folder
if [ ! -d "${BIZNET_SCRIPTS_DIR}" ]; then
  cd "${BIZNET_DIR}" && mkdir scripts && cd scripts
  bash ${TEMPLATE_ROOT_DIR}/initializeScripts.sh \
    "${TEMPLATE_SCRIPTS_DIR}" \
    "${BIZNET_SCRIPTS_DIR}" \
    "${BIZNET_FOLDER_NAME}" \
    "${BIZNET_SRC_DIR}" \
    "${BIZNET_DEPLOY_DIR}" \
    "${BIZNET_TEST_DIR}" \
    "${BIZNET_UI_DIR}"
fi
cd "${BIZNET_DIR}"

#TODO: README.md
if [ ! -f "${BIZNET_DIR}/README.md" ]; then
  cd "${BIZNET_DIR}" && touch README.md
fi
cd "${BIZNET_DIR}"
cp -f "${TEMPLATE_ROOT_DIR}/_Markdown/_README_TEMPLATE.md" "${BIZNET_DIR}/README.md"

cd "${BIZNET_DIR}"

# Loop at entities and create them
IS_FIRST=y
for i in $(jq '.entities | keys | .[]' ${MDL_PARSED_JSON_FILE}); do
# Extract entity properties
  ENTITY=$(jq -r ".entities[$i]" ${MDL_PARSED_JSON_FILE});
  ENTITY_TYPE=$(jq -r '.stereotype' <<< "${ENTITY}");
  ENTITY_NAME=$(jq -r '.name' <<< "${ENTITY}");
  ENTITY_ATTRIBUTES=$(jq -r '.attributes' <<< "${ENTITY}")
# Add attributes of the entity
  case ${ENTITY_TYPE} in

    Asset )
    ASSET_NAME=$(echo ${ENTITY_NAME%Asset})
    ASSET_NAME_LOWER=$(echo ${ASSET_NAME:0:1} | tr '[A-Z]' '[a-z]')${ASSET_NAME:1}
    ASSET_ABBR=$(echo ${ASSET_NAME_LOWER} | tr -d aeiou)
#   Create source directories
    cd "${BIZNET_SRC_DIR}" && \
      mkdir "${ASSET_NAME}${ENTITY_TYPE}" && \
      cd "${ASSET_NAME}${ENTITY_TYPE}" && \
      mkdir src
    cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/gjson.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/gjson.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/gjson_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/gjson_test.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/match.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/match.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/match_test.go" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/match_test.go"
    cp -fi "${TEMPLATE_ASSET_DIR}/SampleAsset.yaml" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
    cp -fi "${TEMPLATE_ASSET_DIR}/chaincode.yaml" "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Enhance the code artifacts (.go, _test.go and .yaml, test, deployment and ui etc.) for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceAssetStructureFromModel.sh \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src" \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go" \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go" \
      "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml" \
      "${BIZNET_TEST_DIR}" \
      "${ASSET_NAME}" \
      "${ASSET_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${TEMPLATE_TEST_DIR}" \
      "${TEMPLATE_UI_DIR}" \
      "${BIZNET_UI_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${TEMPLATE_ROOT_DIR}" \
      "${ENTITY_ATTRIBUTES}"
#   Enhance the deployment descriptor for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityDeployment.sh \
      "${BIZNET_DEPLOY_DIR}" \
      "${ASSET_NAME}" \
      "${ASSET_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${IS_FIRST}"
#   Enhance the test collections / scripts for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityTest.sh \
      "${TEMPLATE_TEST_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${BIZNET_TEST_DIR}" \
      "${ASSET_NAME}" \
      "${ENTITY_TYPE}" \
      "${TEMPLATE_UI_DIR}" \
      "${BIZNET_UI_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${TEMPLATE_ROOT_DIR}"
#   Replace variables - .go file
    sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/sample/'${ASSET_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/smpl/'${ASSET_ABBR}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.go"
#   Replace variables - _test.go file
    sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/sample/'${ASSET_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}_test.go"
#   Replace variables - openAPI.yaml file
    sed -i.bak 's/SampleAsset/'${ASSET_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/Sample/'${ASSET_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/sampleAsset/'${ASSET_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/src/${ASSET_NAME}${ENTITY_TYPE}.yaml"
#   Replace variables - chaincode.yaml file
    sed -i.bak 's/sampleAsset/'${ASSET_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
    sed -i.bak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Delete backup files
    cd "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}"
    find . -name "*.bak" -type f -delete
#   Zip file for chaincode deployment
    zip -rq "${BIZNET_SRC_DIR}/${ASSET_NAME}${ENTITY_TYPE}/${ASSET_NAME}${ENTITY_TYPE}.zip" ./*
    # echo -e "${CYAN}Added ${ENTITY_TYPE}: ${ASSET_NAME}${ENTITY_TYPE} to your business network: ${BIZNET_FOLDER_NAME}${NC}"
    IS_FIRST=n
    continue;;

    Participant )
    PARTICIPANT_NAME=$(echo ${ENTITY_NAME%Participant})
    PARTICIPANT_NAME_LOWER=$(echo ${PARTICIPANT_NAME:0:1} | tr '[A-Z]' '[a-z]')${PARTICIPANT_NAME:1}
    PARTICIPANT_ABBR=$(echo ${PARTICIPANT_NAME_LOWER} | tr -d aeiou)
#   Create source directories
    cd "${BIZNET_SRC_DIR}" && mkdir "${PARTICIPANT_NAME}${ENTITY_TYPE}" && cd "${PARTICIPANT_NAME}${ENTITY_TYPE}" && mkdir src
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/gjson.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/gjson.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/gjson_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/gjson_test.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/match.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/match.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/match_test.go" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/match_test.go"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/SampleParticipant.yaml" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
    cp -fi "${TEMPLATE_PARTICIPANT_DIR}/chaincode.yaml" "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Enhance the code artifacts (.go, _test.go and .yaml, test, deployment and ui etc.) for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceParticipantStructureFromModel.sh \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src" \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go" \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go" \
      "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"  \
      "${BIZNET_TEST_DIR}" \
      "${PARTICIPANT_NAME}" \
      "${PARTICIPANT_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${TEMPLATE_TEST_DIR}" \
      "${TEMPLATE_UI_DIR}" \
      "${BIZNET_UI_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${TEMPLATE_ROOT_DIR}" \
      "${ENTITY_ATTRIBUTES}"
#   Enhance the deployment descriptor for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityDeployment.sh \
      "${BIZNET_DEPLOY_DIR}" \
      "${PARTICIPANT_NAME}" \
      "${PARTICIPANT_NAME_LOWER}" \
      "${ENTITY_TYPE}" \
      "${IS_FIRST}"
#   Enhance the test collections / scripts for this entity
    bash ${TEMPLATE_ROOT_DIR}/enhanceEntityTest.sh \
      "${TEMPLATE_TEST_DIR}" \
      "${BIZNET_FOLDER_NAME}" \
      "${BIZNET_TEST_DIR}" \
      "${PARTICIPANT_NAME}" \
      "${ENTITY_TYPE}"
#   Replace variables - .go file
    sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/sample/'${PARTICIPANT_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
    sed -i.bak 's/smpl/'${PARTICIPANT_ABBR}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.go"
#   Replace variables - _test.go file
    sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
    sed -i.bak 's/sample/'${PARTICIPANT_NAME_LOWER}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}_test.go"
#   Replace variables - openAPI.yaml file
    sed -i.bak 's/SampleParticipant/'${PARTICIPANT_NAME}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/Sample/'${PARTICIPANT_NAME}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
    sed -i.bak 's/sampleParticipant/'${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/src/${PARTICIPANT_NAME}${ENTITY_TYPE}.yaml"
#   Replace variables - chaincode.yaml file
    sed -i.bak 's/sampleParticipant/'${PARTICIPANT_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
    sed -i.bak 's/sample-net/'$BIZNET_FOLDER_NAME'/g' "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/chaincode.yaml"
#   Delete backup files
    cd "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}"
    find . -name "*.bak" -type f -delete
#   Zip file for chaincode deployment
    zip -rq "${BIZNET_SRC_DIR}/${PARTICIPANT_NAME}${ENTITY_TYPE}/${PARTICIPANT_NAME}${ENTITY_TYPE}.zip" ./*
    # echo -e "${CYAN}Added ${ENTITY_TYPE}: ${PARTICIPANT_NAME}${ENTITY_TYPE} to your business network: ${BIZNET_FOLDER_NAME}${NC}"
    IS_FIRST=n
    continue;;

    * )
    break;;

  esac
  continue
done

# Finalize biznet project setup
bash ${TEMPLATE_ROOT_DIR}/finalizeDeployment.sh "${BIZNET_DEPLOY_DIR}"
bash ${TEMPLATE_ROOT_DIR}/finalizeTest.sh "${BIZNET_FOLDER_NAME}" "${BIZNET_TEST_DIR}" "${BIZNET_SCRIPTS_DIR}"
bash ${TEMPLATE_ROOT_DIR}/finalizeSetupUI.sh "${TEMPLATE_UI_DIR}" "${BIZNET_UI_DIR}" "${BIZNET_FOLDER_NAME}"

# biznet setup completion return project directory
printf "${BIZNET_DIR}"
exit 0
