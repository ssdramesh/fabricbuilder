# What's in here?
<Please insert a brief explanation of your business network here>

Use-case Cluster: <Please insert the industry or LoB context for your business network here>

# Context
<Please enter the context on customer(s), consortia, opportunity details etc., here>

# Business Network Model Specification
The business network <bizNetName> contains the following entities

# Model file
![<bizNetName> Data Model](./model/<bizNetName>.png)

# Deployed Systems

[Hyperledger Node Dashboard](https://hyperledger-fabric-dashboard.cfapps.us10.hana.ondemand.com/81ba251c-30bd-40b0-bb12-1b78158439a8/10cb28fb-7ef1-43c5-9a0a-e77b5008af7a/b11946ed-5fa2-482d-9a1e-249816b5b754/node)
[Hyperledger Channel Dashboard](https://hyperledger-fabric-dashboard.cfapps.us10.hana.ondemand.com/81ba251c-30bd-40b0-bb12-1b78158439a8/10cb28fb-7ef1-43c5-9a0a-e77b5008af7a/297b4ab9-587c-408a-943c-2f91023f6b7d/channel)

# Demo URLs
[Demo Application](https://claimnetsetup.cfapps.us10.hana.ondemand.com)

# Demo Script(s)


# Published Links


# Contact(s):
Ramesh Suraparaju
