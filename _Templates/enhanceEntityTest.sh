#!/usr/bin/env bash

TEMPLATE_TEST_DIR=$1
BIZNET_FOLDER_NAME=$2
BIZNET_TEST_DIR=$3
ENTITY_NAME=$4
ENTITY_TYPE=$5

ENTITY_NAME_LOWER=$(echo ${ENTITY_NAME:0:1} | tr "[A-Z]" "[a-z]")${ENTITY_NAME:1}

#newman run script template for the new entity
ENTITY_NEWMAN_RUN_TEMPLATE="

newman run cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json -e ${BIZNET_FOLDER_NAME}.postman_environment.json --bail newman >> ./log/${ENTITY_NAME}${ENTITY_TYPE}slog.txt 2>&1
newman run createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json -e ${BIZNET_FOLDER_NAME}.postman_environment.json -d ./data/${ENTITY_NAME}${ENTITY_TYPE}s.json --bail newman >> ./log/${ENTITY_NAME}${ENTITY_TYPE}slog.txt 2>&1"

#newman environment template for the new entity
ENTITY_NEWMAN_ENV_TEMPLATE="
,
{
  \"key\": \"chaincodeId${ENTITY_NAME}${ENTITY_TYPE}\",
  \"value\": \"<CHAINCODE_ID_${ENTITY_NAME}${ENTITY_TYPE}>\",
  \"enabled\": true,
  \"type\": \"text\"
}"

#Append to backup files
echo "$ENTITY_NEWMAN_RUN_TEMPLATE" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityRunScript.tbak
echo "$ENTITY_NEWMAN_ENV_TEMPLATE" >> ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/entityEnv.tbak

#Copy and replace entity name in the cleanAll POSTMAN Collection for the entity
cp -fi "${TEMPLATE_TEST_DIR}/newman/CleanAllAndSetup/cleanAllEntitys.postman_collection.json" "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/entityLower/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/cleanAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"

#Copy and replace entity name in the createAll POSTMAN Collection for the entity
cp -fi "${TEMPLATE_TEST_DIR}/newman/CleanAllAndSetup/createAllEntitys.postman_collection.json" "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/Entity/'${ENTITY_NAME}${ENTITY_TYPE}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/entityLower/'${ENTITY_NAME_LOWER}${ENTITY_TYPE}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"
sed -i.tbak 's/sample-net/'${BIZNET_FOLDER_NAME}'/g' "${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json"

#Copy the test data into Postman collection
cd ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup
perl -i -p0e 's/<BEGIN_ENTITY_SAMPLE>.*?<END_ENTITY_SAMPLE>/`cat entityCreateAllPostmanCollection.pbak`/seg' ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup/createAll${ENTITY_NAME}${ENTITY_TYPE}s.postman_collection.json

#Delete postman backup
cd ${BIZNET_TEST_DIR}/newman/CleanAllAndSetup
find . -name "*.pbak" -type f -delete

exit 0
