sap.ui.define(function() {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");
	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var entitysDataID = "entitys";

	return {

		init: function(){

			oStorage.put(entitysDataID,[]);
			oStorage.put(
				entitysDataID,
<BEGIN_REPLACE_JSON>
				[
					{
					    "ID"          : "PAR1001",
					    "docType"     : "EntityType.Entity",
					    "alias"       : "India Coffee House",
					    "description" : "SEEPZ, Mumbai"
					},
					{
					    "ID"          : "PAR1002",
					    "docType"     : "EntityType.Entity",
					    "alias"       : "DEMAGDELAG",
					    "description" : "Demag Delewal AG"
					}
				]
<END_REPLACE_JSON>
			);
		},

		getEntityDataID : function(){

			return entitysDataID;
		},

		getEntityData  : function(){

			return oStorage.get("entitys");
		},

		put: function(newEntity){

			var entityData = this.getEntityData();
			entityData.push(newEntity);
			oStorage.put(entitysDataID, entityData);
		},

		patch: function(entity){

			this.remove(entity.ID);
			this.put(entity);
		},

		remove : function (id){

			var entityData = this.getEntityData();
			entityData = _.without(entityData,_.findWhere(entityData,{ID:id}));
			oStorage.put(entitysDataID, entityData);
		},

		removeAll : function(){

			oStorage.put(entitysDataID,[]);
		},

		clearEntityData: function(){

			oStorage.put(entitysDataID,[]);
		}
	};
});
