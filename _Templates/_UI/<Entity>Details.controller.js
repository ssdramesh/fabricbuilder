sap.ui.define([
	"sample-networkSetup/controller/BaseController",
	"sample-networkSetup/util/bizNetAccessEntitys"
], function(
		BaseController,
		bizNetAccessEntitys
	) {
	"use strict";

	return BaseController.extend("sample-networkSetup.controller.EntityDetails", {

		onInit : function(){

			this.getOwnerComponent().getRouter().getRoute("entity").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched : function (oEvent) {

			var pId = oEvent.getParameter("arguments").entityId;
			if ( pId === "___new" ) {
				this.getView().byId("__barEntity").setSelectedKey("New");
			} else {
				bizNetAccessEntitys.loadEntity(this.getView().getModel("Entitys"), oEvent.getParameter("arguments").entityId);
			}
		},

		addNew : function(){

			var oModel = this.getView().getModel("Entitys");
			if ( oModel.getProperty("/newEntity/Alias") !== "" ||
				   oModel.getProperty("/newEntity/Description") !== "" ) {
				var entityId = bizNetAccessEntitys.addNewEntity(this.getOwnerComponent(), oModel);
				this.getView().byId("__barEntity").setSelectedKey("Details");
				bizNetAccessEntitys.loadEntity(this.getView().getModel("Entitys"), entityId);
			} else {
				sap.m.MessageToast.show("Please enter valid data in all fields", {});
			}
			this._clearNewAsset();
		},

		saveEntity : function(){

			var oModel = this.getView().getModel("Entitys");
			bizNetAccessEntitys.updateEntity(this.getOwnerComponent(), oModel);
			this.editEntity();
		},

		removeEntity : function(){

			var oModel = this.getView().getModel("Entitys");
			bizNetAccessEntitys.removeEntity(this.getOwnerComponent(), oModel, oModel.getProperty("/selectedEntity/ID"));
		},

		editEntity : function(oEvent){
/*
		This is ONLY an example of switching editability of an input element
		Please copy and adjust as per the use case requirements to each of the
		fields that are editable after creation.
		After you are done, DELETE this comment!

*/
			var isEditable = this.byId("__inputSelSampleAssetStatus").getEditable();
			if (isEditable === true) {
				this.byId("__inputSelSampleAssetStatus").setEditable(false);
			} else {
				this.byId("__inputSelSampleAssetStatus").setEditable(true);
			}
		},

		_clearNewAsset : function(){

			this.getView().getModel("Entitys").setProperty("/newEntity",{});
		}
	});
});
