sap.ui.define(function() {
	"use strict";

	return {

		mapEntityToModel:function(responseData){
			return {
				ID:responseData.ID,
				ObjectType:responseData.docType,
				<BEGIN_REPLACE_ENTITY_TO_MODEL>

				<END_REPLACE_ENTITY_TO_MODEL>
			};
		},

		mapEntitysToModel:function(responseData){

			var items = [];
			if( responseData ){
				for ( var i = 0; i < responseData.length; i++ ){
					items.push(this.mapEntityToModel(responseData[i]));
				}
			}
			return items;
		},

		mapEntityToChaincode:function(oModel, newEntity){

			if ( newEntity === true ) {
				return {
						"ID":this.getNewEntityID(oModel),
						"docType":"EntityType.Entity",
						<BEGIN_REPLACE_ENTITY_TO_CHAINCODE_NEW>

						<END_REPLACE_ENTITY_TO_CHAINCODE_NEW>
				};
			} else {
				return {
						"ID":oModel.getProperty("/selectedEntity/ID"),
						"docType":"EntityType.Entity",
						<BEGIN_REPLACE_ENTITY_TO_CHAINCODE_SEL>

						<END_REPLACE_ENTITY_TO_CHAINCODE_SEL>
				};
			}
		},

		mapEntityToLocalStorage : function(oModel, newEntity){

			return this.mapEntityToChaincode(oModel, newEntity);
		},

		getNewEntityID:function(oModel){

		    if ( typeof oModel.getProperty("/newEntity/ID") === "undefined" ||
		    		oModel.getProperty("/newEntity/ID") === ""
		    	){
			    var iD = "Entity";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			    for( var i = 0; i < 8; i++ ){
			        iD += possible.charAt(Math.floor(Math.random() * possible.length));
			    }
		    } else {
				iD = oModel.getProperty("/newEntity/ID");
			}
			oModel.setProperty("/newEntity/ID",iD);
		    return iD;
		}
	};
});
