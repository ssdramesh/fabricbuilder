sap.ui.define([
	"sample-networkSetup/controller/BaseController",
	"sample-networkSetup/model/modelsBase",
	"sample-networkSetup/util/messageProvider",
	"sample-networkSetup/util/localStoreEntitys",
	"sample-networkSetup/util/bizNetAccessEntitys",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/Device"
], function(
		BaseController,
		modelsBase,
		messageProvider,
		localStoreEntitys,
		bizNetAccessEntitys,
		History,
		Filter,
		FilterOperator,
		Device
	) {
	"use strict";

	return BaseController.extend("sample-networkSetup.controller.EntitysOverview", {

		onInit : function(){

			var oList = this.byId("entitysList");
			this._oList = oList;
			this._oListFilterState = {
				aFilter : [],
				aSearch : []
			};
			localStoreEntitys.init();
			if ( typeof this.getOwnerComponent().getModel("Messages") === "undefined" ){
				this.getOwnerComponent().setModel(modelsBase.createMessagesModel(), "Messages");
			}
			messageProvider.init(this.getOwnerComponent().getModel("Messages"));
		},

		onSelectionChange : function (oEvent) {

			this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		},

		onAdd : function() {

			this.getRouter().navTo("entity", {entityId : "___new"});
		},

		onRemoveAll : function(){

			bizNetAccessEntitys.removeAllEntitys(this.getOwnerComponent(), this.getOwnerComponent().getModel("Entitys"));
		},

		onSearch : function(oEvent) {

			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				this._oListFilterState.aSearch = [new Filter("ID", FilterOperator.Contains, sQuery)];
			} else {
				this._oListFilterState.aSearch = [];
			}
			this._applyFilterSearch();
			var bReplace = !Device.system.phone;
			var oModel = this.getModel("Entitys");
			oModel.setProperty(
				"/selectedEntity",
				_.findWhere(oModel.getProperty("/entityCollection/items"),
					{
						ID: oModel.getProperty("/searchEntityID")
					},
				this));
			this.getRouter().navTo("entity", {
				entityId : oModel.getProperty("/selectedEntity").ID
			}, bReplace);
		},

		_onToggleBaaS : function(){

			this.getOwnerComponent().getModel("TestSwitch").setProperty("/testMode",!(this.getView().byId("__buttonToggleEntitys").getPressed()));
			var location = this.getRunMode().location;
			messageProvider.addMessage("Success", "Switching Network for Blockchain Data to: ", "No Description", location, 1, "", "http://www.sap.com");
			if ( this.getRunMode().testMode && localStoreEntitys.getEntityData() === null ){
				localStoreEntitys.init();
			}
			bizNetAccessEntitys.loadAllEntitys(this.getOwnerComponent(), this.getModel("Entitys"));
		},

		_showDetail : function(oItem) {

			var bReplace = !Device.system.phone;
			var oModel = this.getModel("Entitys");
			this.getRouter().navTo("entity", {
				entityId : oModel.getObject(oItem.getBindingContextPath()).ID
			}, bReplace);
		},

		_applyFilterSearch : function () {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("Entitys");
			this._oList.getBinding("items").filter(aFilters, "Application");
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("_labelNoEntitysText"));
			}
		},

		onMessagePress: function() {

			this.onShowMessageDialog("sample-networkSetup Log");
		},

		onNavBack : function() {
			this.getRouter().navTo("home");
		}
	});
});
