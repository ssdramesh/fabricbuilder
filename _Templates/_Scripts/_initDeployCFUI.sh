#!/bin/bash

cd <BIZNET_SETUP_UI_DIR>
echo
echo -e "Installing dependencies for node.js buildpack...Please enter your super user (sudo) password..."
sudo npm install --save
sudo npm install --package-lock-only
sudo npm audit fix
echo
cf login -a <API_ENDPOINT>
cf push
