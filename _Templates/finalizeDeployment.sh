#!/usr/bin/env bash

BIZNET_DEPLOY_DIR=${1}
cd "${BIZNET_DEPLOY_DIR}"

#Close Entity Collection and append to backup file
printf "]\n}" >> ${BIZNET_DEPLOY_DIR}/entitiesDeployment.dbak
#Append the contents of deployment backup file to the target deployment descriptor file
cat ${BIZNET_DEPLOY_DIR}/entitiesDeployment.dbak >> chaincodeDetails.json

#Check and delete previous states of configuration files
find . -name "*.dbak" -type f -delete
exit 0
