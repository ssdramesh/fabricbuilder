const {app, BrowserWindow, dialog} = require("electron");
require("electron-debug")({ enabled : true, showDevTools: process.env.NODE_ENV === 'development' })

var electronDirectory = require("electron-directory");
var sync = require("promise-synchronizer");
var path = require("path");

let getIndexFilePath = new Promise((resolve, reject) => {
  electronDirectory(__dirname)
    .then(function(electronDirectoryInstance){
      electronDirectoryInstance.getApplicationPath("index.html")
        .then( function(indexFilePath) {
          resolve(indexFilePath);
        })
    })
})

let indexFile;
try {
  indexFile = sync(getIndexFilePath);
} catch(err) {
  console.error(err);
}

let win;

function createWindow () {
  win = new BrowserWindow(
    {
      width:1600,
      height:900,
      minWidth: 1500,
      minHeight: 800,
      webPreferences:{
        nodeIntegration: true
      }
    });
  // win.webContents.openDevTools();
  win.loadFile(indexFile);
  win.on("closed", () => {
    win = null;
  })
}

app.on("ready", createWindow);
app.on("window-all-closed", function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
})
