var path = require("path");
var read = require("read-data").sync;
var fs = require("fs");
var cp = require("cp").sync;
var replace = require("replace-in-file").sync;
var folderAndFiles = require("../fsUtil/folderAndFiles.js");

async function getToken(projectName, generationProjectPath, cfChannelDetails) {

  var channelAccessFile = generationProjectPath + "/deployment" + "/ChannelDetails/" + cfChannelDetails;
  var serviceKey = read(channelAccessFile);
  var authBasic = btoa(serviceKey.oAuth.clientId + ":" + serviceKey.oAuth.clientSecret);

  var request = require("request-promise");
  var options = {
    url: serviceKey.oAuth.url + "/oauth/token?grant_type=client_credentials",
    method: "GET",
    headers:{
      "Authorization": "Basic " + authBasic,
      "Accept": "application/json"
    },
    json: true
  };

  let data = await request(options);
  return data.access_token;
}

async function deployChaincodes(projectName, generationProjectPath, cfChannelDetails) {

  var deployedChaincodes=[];
  var bizNetSrcDir = generationProjectPath + "/chaincode";
  var channelAccessFile = generationProjectPath + "/deployment" + "/ChannelDetails/" + cfChannelDetails;

  var deployDescrFileName = generationProjectPath + "/deployment/chaincodeDetails.json";
  var pmTestEnvFileName = generationProjectPath + "/test/newman/CleanAllAndSetup/" + projectName + ".postman_environment.json";
  var uiDeployDescrFileName = generationProjectPath + "/ui/" + projectName + "Setup/webapp/model/chaincodeDetails.json";
  var uiScriptFileName = generationProjectPath + "/scripts/deployCFUI.sh";
  var uiManifestFileName = generationProjectPath + "/ui/" + projectName + "Setup/manifest.yaml";

  var serviceKey = read(channelAccessFile);
  var region = serviceKey.serviceUrl.split(".")[2];
  var apiEndpoint = "https://api.cf." + region + ".hana.ondemand.com";
  var hlfChannelInstanceName = cfChannelDetails.substring(0, cfChannelDetails.lastIndexOf("."));
  var deployUrl = read(channelAccessFile).serviceUrl + "/chaincodes";

  cp(deployDescrFileName, uiDeployDescrFileName);
  
  replace({
    files: [pmTestEnvFileName, deployDescrFileName, uiDeployDescrFileName],
    from: [/<DEPLOY_URL>/g, /<IDENTITY_ZONE>/g, /<REGION>/g, /<CLIENT_ID>/g, /<CLIENT_SECRET>/g, /<CHANNEL_SERVICE_INSTANCE>/g],
    to: [deployUrl, serviceKey.oAuth.identityZone, region, serviceKey.oAuth.clientId, serviceKey.oAuth.clientSecret, hlfChannelInstanceName],
  });

  replace({
    files: [uiScriptFileName, uiManifestFileName],
    from: [/<API_ENDPOINT>/g, /<CHANNEL_SERVICE_INSTANCE>/g],
    to: [apiEndpoint, hlfChannelInstanceName],
  });

  let token = await getToken(projectName, generationProjectPath, cfChannelDetails);
  var entityFolders = folderAndFiles.getDirectories(bizNetSrcDir);
  for ( var i = 0; i < entityFolders.length; i++ ) {
    var entityName = path.basename(entityFolders[i]);
    var cleanScriptFileName = generationProjectPath + "/test/newman/CleanAllAndSetup/cleanAll" + entityName + "s.postman_collection.json";
    var createScriptFileName = generationProjectPath + "/test/newman/CleanAllAndSetup/createAll" + entityName + "s.postman_collection.json";
    var deployFile = folderAndFiles.fromDir(entityFolders[i],".zip");
    var request = require("request-promise");
    let data = await request.post({
      url: serviceKey.serviceUrl + "/chaincodes",
      formData: {
        arguments: "[]",
        file: fs.createReadStream(deployFile),
      },
      headers:{
        "Authorization": "Bearer " + token,
        "Accept": "application/json",
        "Content-Type": "multipart/form-data"
      },
      json: true
    });

    deployedChaincodes.push({
        "entityName": entityName,
        "chaincodeId": data.chaincodeId,
        "version": data.version
    });
    // Insert chaincodeId in relevant generated files
    replace({
      files: [deployDescrFileName, pmTestEnvFileName, uiDeployDescrFileName, cleanScriptFileName, createScriptFileName],
      from: ["<CHAINCODE_ID_" + entityName + ">", "<CHAINCODE_VERSION_" + entityName + ">"],
      to: [data.chaincodeId, data.version],
    });
  }
  return deployedChaincodes;
}

exports.deployChaincodes = deployChaincodes;
