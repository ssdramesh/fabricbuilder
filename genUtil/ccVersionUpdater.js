var fs = require("fs");
var archiver = require("archiver");
var replace = require("replace-in-file").sync;
var _ = require("underscore");
var config = require("../configUtil/config.js");
var folderAndFiles = require("../fsUtil/folderAndFiles.js");


function incrementVersions(projectName) {
  var project = config.getProject(projectName);
  var ccSrcFolder = project.generationProjectPath + "/chaincode";
  var entityFolders = folderAndFiles.getDirectories(ccSrcFolder);

  var deployedChaincodes = project.deployedChaincodes;

  for ( i = 0; i < entityFolders.length; i++ ) {
    var entityName = path.basename(entityFolders[i]);
    // Delete chaincode Zip
    fs.unlinkSync(entityFolders[i] + "/" + entityName + ".zip");
    // Get current version
    var cVer = parseFloat(_.findWhere(deployedChaincodes, {entityName:entityName}).version);
    var nVer = parseFloat(cVer + 0.01);
    // Replace existing version with incremented version in chaincode.yaml
    replace({
      files: [entityFolders[i] + "/chaincode.yaml"],
      from: [cVer],
      to: [nVer],
    });
    // Rezip chaincode Zip
    var output = fs.createWriteStream(ccSrcFolder + "/" + entityName + "/" + entityName + ".zip");
    var archive = archiver("zip");
    output.on("close", function() {
      console.log(archive.pointer() + " total bytes");
      console.log("archiver has been finalized and the output file descriptor has closed.");
    });
    output.on("end", function() {
      console.log('Data has been drained');
    });
    archive.on("warning", function(err) {
      if (err.code === 'ENOENT') {
        console.log(err)
      } else {
        throw err;
      }
    });
    archive.on("error", function(err) {
      throw err;
    });

    archive.pipe(output);
    // archive.bulk([
    //     { expand: true, cwd: entityFolders[i], src: ['**'], dest: entityFolders[i]}
    // ]);
    archive.append(fs.createReadStream(entityFolders[i]+"/chaincode.yaml"), { name: 'chaincode.yaml' });
    archive.directory(entityFolders[i]+"/src", "src");
    archive.finalize();
  }
}

exports.incrementVersions = incrementVersions;
