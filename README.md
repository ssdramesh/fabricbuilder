# FabricBuilder

RAD tool for generating, deploying chaincode entities on Hyperledger Fabric instances and generating deploying SAPUI5 applications to work with chaincode data models.

# Installation
CAUTION: At present this distribution only works on MacOS.
The application uses node modules that render a canvas for modeling your chaincode data-models.
If you did not work with cross-platform (node.js / electron-based) modeling tools like StarUML before, you need to install a few macOS casks for enabling the node modules like canvas and fabric to run smoothly before you install the .dmg file.

```
brew install pkg-config cairo pango libpng jpeg giflib librsvg
```

## MacOS
