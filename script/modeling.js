var entityModel = require("./modelUtil/entityModel.js");

document.getElementById("loadModel").addEventListener("click", function() {
  entityModel.loadModel();
});

document.getElementById("addNewAsset").addEventListener("click", function() {
  entityModel.addEntity("Asset");
});

document.getElementById("addNewParticipant").addEventListener("click", function() {
  entityModel.addEntity("Participant");
});

document.getElementById("saveModel").addEventListener("click", function() {
  entityModel.saveModel();
});
