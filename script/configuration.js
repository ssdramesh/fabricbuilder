var {dialog} = require('electron').remote;
var electronDirectory = require("electron-directory");
var fs = require("fs-extra");
var configUtil = require("./configUtil/config.js");

document.getElementById("scriptsLocation").value = configUtil.readScriptPathSetting();
document.getElementById("modelsLocation").value = configUtil.readModelPathSetting();

function getSelectedDirectory() {
  mainProcess.selectDirectory();
}

// Fill table of existing projects
fillTableChannels();

/*
Fills table with existing projects
 */
function fillTableChannels() {
  var table = document.getElementById("tableChannels");
  var tableChannels = document.createElement("table");
  addChannelTableHead(tableChannels);
  electronDirectory(__dirname)
    .then( function(electronDirectoryInstance) {
      electronDirectoryInstance.getApplicationPath("/_Templates/_Deployment/ChannelDetails")
        .then( function(appPath) {
          // console.log(appPath);
          fs.readdirSync(appPath).forEach(file => {
            addChannelTableRow(tableChannels, file)
        })
        table.appendChild(tableChannels);
      });
    })
}
/*
Fills existing projects table header
 */
function addChannelTableHead(tableChannels) {
  var head = document.createElement("thead");
  var headerRow = document.createElement("tr");
  var cellChannelName = document.createElement("td");
  var cellChannelAction = document.createElement("td");
  cellChannelName.appendChild(document.createTextNode("Channel Name"));
  cellChannelAction.appendChild(document.createTextNode("Action"))
  headerRow.appendChild(cellChannelName);
  headerRow.appendChild(cellChannelAction);
  head.appendChild(headerRow);
  tableChannels.appendChild(head);
}
/*
Build and add one row each for existing projects in the configurations table
 */
function addChannelTableRow(tableChannels, channelName) {
  var delButton = document.createElement("Button");
  delButton.id = projectName;
  delButton.innerHTML = "Delete";
  delButton.addEventListener("click", function() {
    deleteChannel(channelName);
  })
  var row = document.createElement("tr");
  var cellChannelName = document.createElement("td");
  var cellChannelAction = document.createElement("td");
  cellChannelName.appendChild(document.createTextNode(channelName));
  cellChannelAction.appendChild(delButton);
  row.appendChild(cellChannelName);
  row.appendChild(cellChannelAction);
  tableChannels.appendChild(row);
}

function createChannel(channelName, channelJSON){
  var channelsFolder = confConfigUtil.readScriptPathSetting() + "/_Deployment/ChannelDetails";
  write(channelsFolder + "/" + channelName, JSON.parse(channelJSON));
}

function deleteChannel(channelName) {
  var channelsFolder = confConfigUtil.readScriptPathSetting() + "/_Deployment/ChannelDetails";
  fs.unlinkSync(channelsFolder + "/" + channelName);
  var tableChannels = document.getElementById("tableChannels")
  while(tableChannels.hasChildNodes()) {
     tableChannels.removeChild(tableChannels.firstChild);
  }
  fillTableChannels();
  dialog.showMessageBox({
    title: "Delete Channel",
    buttonLabel: "Delete",
    message: "Channel Deleted" + channelName
  });
}

/*
Add new channel
 */
document.getElementById("addNewChannelWithKey").addEventListener("click", () => {
  var channelName = document.getElementById("newChannelName").value;
  var channelJSON = document.getElementById("newChannelKey").value;
  if ( channelName === "" || channelJSON === "" ) {dialog.showErrorBox("Incomplete Specification", "Specify all Parameters");}
  createChannel(channelName, channelJSON);
});
