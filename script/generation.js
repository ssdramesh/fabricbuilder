var read = require("read-data").sync;
var fs = require("fs-extra");
var generator = require("./genUtil/generator.js");

fillAllSelections();

function fillProjectSelections() {
    var projectName = document.getElementById("projectName");
    read("./config/config.json").projects.forEach(project => {projectName.options[projectName.options.length] = new Option(project.projectName, project.projectName);});
}

function fillChannelSelections() {
    var cfChannelDetails = document.getElementById("cfChannelDetails");
    fs.readdirSync("./_Templates/_Deployment/ChannelDetails").forEach(file => {cfChannelDetails.options[cfChannelDetails.options.length] = new Option(file, file);});
}

function fillAllSelections() {
  fillProjectSelections();
  fillChannelSelections();
}
/*
Add event listener for refresh projects button
 */
document.getElementById("refreshProjectList").addEventListener("click", () => {fillProjectSelections();});
/*
Add event listener for refresh cloud foundry channel details button
 */
document.getElementById("refreshCfChannelDetails").addEventListener("click", () => {fillChannelSelections();});
/*
Add event listener for generate project button
 */
document.getElementById("generateProject").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.generateProject(projectName.options[projectName.selectedIndex].value);
});
/*
Add event listener for deploy project button
 */
document.getElementById("deployProject").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.deployProject(projectName.options[projectName.selectedIndex].value, cfChannelDetails.options[cfChannelDetails.selectedIndex].value);
});
/*
Add event listener for generate sample data buttin
 */
document.getElementById("generateData").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.generateSampleData(projectName.options[projectName.selectedIndex].value);
});
