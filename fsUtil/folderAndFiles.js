var { lstatSync, readdirSync } = require("fs");
var { join } = require("path");


const isDirectory = source => lstatSync(source).isDirectory()
const getDirectories = source => readdirSync(source).map(name => join(source, name)).filter(isDirectory)

function fromDir(startPath,filter){
    if (!fs.existsSync(startPath)){ return; }
    var files = readdirSync(startPath);
    var filename = "";
    for( var i = 0; i < files.length; i++ ){
        filename=join(startPath,files[i]);
        if (isDirectory(filename)){continue;}
        else if (filename.indexOf(filter)>=0) {break;}; //This is initial deployment, so only one ".zip" chaincode file must exist!!
    };
    return filename;
};

exports.getDirectories = getDirectories;
exports.fromDir = fromDir;
